class ResourcesManager(dict):
    def __init__(self, res_type, data_store, data_table):
        self.res_type = res_type
        self.config_db = data_store
        self.config_table = data_table

    def get(self, key):
        print("deprecated use dictionary syntax ('instance[key]') instead")
        raise AttributeError

    def save_to_config(self):
        for res in self.values():
            res.save_to_config(self.config_db, self.config_table)

