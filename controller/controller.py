import logging
from io_components.base.connector import Connector, State
from io_components.base.variable import Variable
from io_components.base.binding import Binding
from io_components.base.condition import *
from io_components.base.action import *
from controller.resources_manager import ResourcesManager
from io_components import *

class Controller:
    def __init__(self, data_store):

        self.data_store = data_store

        # create resource managers
        self.connectors = ResourcesManager(Connector, self.data_store, 'connectors')
        self.variables = ResourcesManager(Variable, self.data_store, 'variables')
        self.bindings = ResourcesManager(Binding, self.data_store, 'bindings')
        self.conditions = ResourcesManager(Condition, self.data_store, 'conditions')
        self.actions = ResourcesManager(Action, self.data_store, 'actions')

        # log application start
        logging.info("Application started")

    def add_connector(self, connector):
        return self.connectors.add(connector)

    def get_connector(self, key):
        return self.connectors[key]

    def add_variable(self, variable):
        return self.variables.add(variable)

    def get_variable(self, key):
        return self.variables[key]

    def add_binding(self, binding):
        return self.bindings.add(binding)

    def get_binding(self, key):
        return self.bindings[key]

    def load(self):
        #   create all connectors
        for c in Connector.get_all(self.data_store):
            cc = Connector.get_dict_from_config(self.data_store, c)
            logging.debug(cc)
            ccc = create_implementation(**cc)
            self.connectors[ccc.name] = ccc

        #   create all bindings
        for b in Binding.get_all(self.data_store):
            bb = Binding.get_dict_from_config(self.data_store, b)
            logging.debug(bb)
            bb['connector'] = self.connectors[bb['connector']]
            bbb = create_implementation(**bb)
            self.bindings[bbb.name] = bbb

        #   create all variables
        for v in Variable.get_all(self.data_store):
            vv = Variable.get_dict_from_config(self.data_store, v)
            logging.debug(vv)
            if vv['binding'] is not None:
                vv['binding'] = self.get_binding(vv['binding'])
            vvv = create_implementation(**vv)
            self.variables[vvv.name] = vvv

        #   create all conditions
        for c in Condition.get_all(self.data_store):
            cc = Condition.get_dict_from_config(self.data_store, c)
            logging.debug(cc)
#            print(self.variables[cc['variable']])
            cc['variable'] = self.variables[cc['variable']]
            ccc = create_implementation(**cc)
            self.conditions[ccc.name] = ccc

        #   create all actions
        for a in Action.get_all(self.data_store):
            aa = Action.get_dict_from_config(self.data_store, a)
            logging.debug(aa)
            aaa = create_implementation(**aa)
            self.actions[aaa.name] = aaa

    def connect(self):
        for v in self.variables:
            var = self.variables[v]
            if var.binding is not None:
                var.binding.connector.connect()
                if var.binding.connector.state == State.connected:
                    var.binding.start_polling()
                else:
                    error_msg = "Connector {} was unable to connect".format(var.binding.connector.name)
                    logging.error(error_msg)
                    print(error_msg)
