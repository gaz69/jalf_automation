import logging
import json
import sqlite3
from io_components import create_implementation
from io_components.base.binding import Binding
from io_components.base.connector import Connector
from io_components.base.variable import Variable
from io_components.base.condition import Condition
from io_components.base.action import Action

class Sqlite3DataStore:

    def __init__(self, db_file):
        self.db = sqlite3.connect(db_file)  # './app_data/resources.db'
        self.db_file = db_file
        pass

    # Bindings
    def get_all_bindings(self):
        """
    Gets a dictionary with all bindings in the configuration database.
    :return: a dictionary with the all binding names as keys and None as value.
    """
        cursor = self.db.cursor()
        cursor.execute('select name from binding')
        result = {i[0]: None for i in cursor}
        return result

    def update_binding(self, binding, data):
        """
        Updates the binding configuration data in the configuration database.
        :param binding: binding object to be saved.
        :param data: binding specific data.
        :return: True on success, false otherwise.
        """
        rowcount = 0
        result = False
        cursor = self.db.cursor()
        class_name = type(binding).__name__
        try:
            cursor.execute(
                """ update {}
                    set
                    class_name = ?,
                    connector = ?,
                    data = ?
                    where name = ?""".format(Binding.table),
                (class_name, binding.connector.name, data, binding.name))
            rowcount = cursor.rowcount
            if rowcount > 0:
                self.db.commit()
                result = True
        finally:
            logging.debug(rowcount)

        return result

    def save_binding(self, binding, data):
        """
        Creates (inserts) a new binding in the configuration database.
        :param binding: connector object to be saved.
        :param data: binding specific data.
        :return: true on success, false otherwise.
        """
        result = False
        cursor = self.db.cursor()
        class_name = type(self).__name__
        try:
            cursor.execute(
                """ insert into {} 
            (name, 
             class_name,
             connector,
             data
            )
            values (?,?,?,?)
         """.format(Binding.table),
                (binding.name, class_name, binding.connector.name, data))
            rowcount = cursor.rowcount
            if rowcount > 0:
                self.db.commit()
                result = True
        except:
            pass

        return result

    def get_binding(self, name):
        """
        Creates a binding object from a binding stored in the configuration database.
        :param name: binding name.
        :return: a binding object.
        """
        config_data = self.get_binding_dict(name)
        if config_data is not None:
            args = {a[0]: a[1]
                    for a in config_data.items()
                    if a[0] not in ['class_name', 'connector']}
            if config_data['connector'] != '':
                connector = Connector.create_from_config(self, config_data['connector'])
                args.update({'connector': connector})

            return create_implementation(config_data['class_name'], **args)
        else:
            return None
    def get_binding_dict(self, name):
        """
        Gets the configuration of a binding as saved in the configuration database.
        :param name: the name of the binding whose information is to be fetched.
        :return: a dictionary with the current information in the configuration database
        or None if the binding does not exist.
        """
        db_cursor = self.db.cursor()
        db_cursor.execute("""select name,
                          class_name,
                          connector,
                          data
                          from {}
                          where name = ?""".format(Binding.table),
                          (name,))
        data = db_cursor.fetchone()
        if data is not None:
            result = {'name': data[0],
                      'class_name': data[1],
                      'connector': data[2]}
#            result.update({"data_store": "SQLite3 file:{}".format(self.db_file)})
            result.update(json.loads(data[3]))
            return result
        else:
            return None

    # Connectors
    def update_connector(self, connector, data):
        """
        Updates the connector configuration data in the configuration database.
        :param connector: connector object to be saved.
        :param data: connector specific data.
        :return: True on success, false otherwise.
        """
        result = False
        cursor = self.db.cursor()
        class_name = type(connector).__name__
        rowcount = 0
        try:
            cursor.execute(
                """ update {}
              set
              class_name = ?,
              data = ?
              where name = ?""".format(Connector.table),
                (class_name, data, connector.name))
            rowcount = cursor.rowcount
            if rowcount > 0:
                self.db.commit()
                result = True
        finally:
            logging.debug(rowcount)
        return result

    def save_connector(self, connector, data):
        """
        Creates (inserts) a new connector in the configuration database.
        :param connector: connector object to be saved.
        :param data: connector specific data.
        :return: true on success, false otherwise.
        """
        result = False
        cursor = self.db.cursor()
        class_name = type(connector).__name__
        try:
            cursor.execute(
                """ insert into {} 
              (name, 
               class_name,
               data
              )
              values (?,?,?)
              """.format(Connector.table),
                (connector.name, class_name, data))
            rowcount = cursor.rowcount
            if rowcount > 0:
                self.db.commit()
                result = True
        except:
            pass
        return result

    def create_connector(self, name):
        """
        Creates a connector object from a connector stored in the configuration database.
        :param name: connector name.
        :return: a connector object.
        """
        config_data = self.get_connector_dict(name)
        args = {a[0]: a[1] for a in config_data.items() if a[0] != 'class_name'}

        return create_implementation(config_data['class_name'], **args)

    def get_connector_dict(self, name):
        """
        Gets the configuration of a connector as saved in the configuration database.
        :param name: the name of the connector whose information is to be fetched.
        :return: a dictionary with the current information in the configuration database
        or None if the variable does not exist.
        """
        db_cursor = self.db.cursor()
        db_cursor.execute("""select name,
                          class_name,
                          data
                          from {}
                          where name = ?""".format(Connector.table),
                          (name,))
        data = db_cursor.fetchone()
        if data is not None:
            result = {'name': data[0], 'class_name': data[1]}
            result.update({"data_store": "SQLite3 file:{}".format(self.db_file)})
            result.update(json.loads(data[2]))
            return result
        else:
            return None

    def get_all_connectors(self):
        """
        Gets a dictionary with all connectors in the configuration database.
        :return: a dictionary with the all connector names as keys and None as value.
        """
        cursor = self.db.cursor()
        cursor.execute('select name from connector')
        result = {i[0]: None for i in cursor}
        return result

    # Variables

    def update_variable(self, variable, data=''):
        """
        Updates the variable configuration data in the configuration database.
        :param variable: variable object to be saved.
        :param data: variable specific data.
        :return: True on success, false otherwise.
        """
        logging.debug("variable: {}".format(variable.__dict__))
        result = False
        cursor = self.db.cursor()
        class_name = type(variable).__name__
        rowcount = 0
        try:
            if variable.binding is not None:
                cursor.execute(
                    """update {}
                        set
                        class_name = ?,
                        var_type = ?,
                        binding = ?,
                        persistent = ?,
                        data = ?
                        where name = ?""".format(Variable.table),
                    (class_name, variable.var_type, variable.binding.name, variable.persistent, data, variable.name))
            else:
                cursor.execute(
                    """ update {}
                        set
                        class_name = ?,
                        var_type = ?,
                        binding = NULL,
                        persistent = ?,
                        data = ?
                        where name = ?""".format(Variable.table),
                    (class_name, variable.var_type, variable.persistent, data, variable.name))
                rowcount = cursor.rowcount
            if rowcount > 0:
                self.db.commit()
                result = True
        finally:
            logging.debug(rowcount)
        return result

    def save_variable(self, variable, data=''):
        """
        Creates (inserts) a new variable in the configuration database.
        :param variable: variable object to be saved.
        :param data: variable specific data.
        :return: true on success, false otherwise.
        """

        rowcount = 0
        result = False
        cursor = self.db.cursor()
        class_name = type(variable).__name__
        try:
            if variable.__binding is not None:
                cursor.execute(
                    """ insert into {} 
                    (name, 
                    class_name,
                    var_type,
                    binding,
                    persistent,
                    data
                    )
                    values (?,?,?,?,?,?)
                    """.format(Variable.table),
                    (variable.name, class_name, variable.var_type, variable.__binding.name, variable.persistent, data))
            else:
                cursor.execute(
                    """ insert into {} 
                    (name, 
                    class_name,
                    var_type,
                    persistent,
                    data
                    )
                    values (?,?,?,?,?)
                    """.format(Variable.table),
                    (variable.name, class_name, variable.var_type, variable.persistent, data))
                rowcount = cursor.rowcount
            if rowcount > 0:
                self.db.commit()
                result = True
        except:
            pass
        return result

    def get_variable_dict(self, name):
        """
        Gets the configuration of a variable as saved in the configuration database.
        :param name: the name of the variable whose information is to be fetched.
        :return: a dictionary with the current information in the configuration database
        or None if the variable does not exist.
        """
        db_cursor = self.db.cursor()
        db_cursor.execute("""select name,
                            class_name,
                            var_type,
                            binding,
                            persistent,
                            data
                            from {}
                            where name = ?""".format(Variable.table),
                          (name,))
        data = db_cursor.fetchone()
        if data is not None:
            result = {'name': data[0],
                      'class_name': data[1],
                      'var_type': data[2],
                      'binding': data[3],
                      'persistent': data[4],
                      }
            if len(data) > 5:
                if (data[5] is not None and
                        len(data[5]) > 0):
                    result.update(json.loads(data[5]))
#            result.update({"data_store": "SQLite3 file:{}".format(self.db_file)})
            return result
        else:
            return None

    def get_all_variables(self):
        """
        Gets a dictionary with all variables in the configuration database.
        :return: a dictionary with the all variable names as keys and None as value.
        """
        cursor = self.db.cursor()
        cursor.execute('select name from variable')
        result = {i[0]: None for i in cursor}
        return result

    def get_variable(self, name):
        """
        Creates a variable object from a variable stored in the configuration database.
        :param name: variable name.
        :return: a variable object.
        """
        config_data = Variable.get_dict_from_config(self, name)
        if config_data is not None:
            args = {a[0]: a[1]
                for a in config_data.items()
                if a[0] not in ['class_name', 'binding']}
            if config_data['binding'] != '':
                binding = Binding.create_from_config(self, config_data['binding'])
                args.update ({'binding': binding})

            return create_implementation(config_data['class_name'], **args)
        else:
            return None

    # Conditions

    def update_condition(self, condition):
        """
        Updates the condition configuration data in the configuration database.
        :param condition: condition object to be saved.
        :return: True on success, false otherwise.
        """
        logging.debug("condition: {}".format(condition.__dict__))
        result = False
        cursor = self.db.cursor()
        class_name = type(condition).__name__
        rowcount = 0
        try:
            if condition.variable is not None:
                cursor.execute(
                    """update {}
                        set
                        class_name = ?,
                        variable = ?,
                        ref_value = ?,
                        value_type = ?,
                        check_once = ?,
                        check_interval = ?
                        where name = ?""".format(Condition.table),
                    (class_name, condition.variable.name, condition.ref_value,
                     condition.value_type, condition.check_once, condition.check_interval, condition.name))
            else:
                cursor.execute(
                    """ update {}
                        set
                        class_name = ?,
                        variable = NULL,
                        ref_value = ?,
                        value_type = ?,
                        check_once = ?,
                        check_interval = ?
                        where name = ?""".format(Condition.table),
                    (class_name, condition.ref_value, condition.value_type,
                     condition.check_once, condition.interval, condition.name))
            rowcount = cursor.rowcount
            if rowcount > 0:
                self.db.commit()
                result = True
        finally:
            logging.debug(rowcount)
        return result

    def save_condition(self, condition):
        """
        Creates (inserts) a new condition in the configuration database.
        :param condition: condition object to be saved.
        :return: true on success, false otherwise.
        """

        rowcount = 0
        result = False
        cursor = self.db.cursor()
        class_name = type(condition).__name__
        try:
            if condition.__variable is not None:
                cursor.execute(
                    """ insert into {} 
                    (name, 
                    class_name,
                    variable,
                    ref_value,
                    value_type,
                    check_once,
                    check_interval
                    )
                    values (?,?,?,?,?,?,?)
                    """.format(Condition.table),
                    (condition.name, class_name, condition.variable.name, condition.ref_value,
                     condition.value_type, condition.check_once, condition.check_interval))
            else:
                cursor.execute(
                    """ insert into {} 
                    (name, 
                    class_name,
                    ref_value,
                    value_type,
                    check_once,
                    check_interval
                    )
                    values (?,?,?,?,?)
                    """.format(Condition.table),
                    (condition.name, class_name, condition.ref_value,
                     condition.value_type,condition.check_once,condition.check_interval))
            rowcount = cursor.rowcount
            if rowcount > 0:
                self.db.commit()
                result = True
        except:
            pass
        return result

    def get_condition_dict(self, name):
        """
        Gets the configuration of a condition as saved in the configuration database.
        :param name: the name of the condition whose information is to be fetched.
        :return: a dictionary with the current information in the configuration database
        or None if the variable does not exist.
        """
        db_cursor = self.db.cursor()
        db_cursor.execute("""select name,
                            class_name,
                            variable,
                            ref_value,
                            value_type,
                            check_once,
                            check_interval
                            from {}
                            where name = ?""".format(Condition.table),
                          (name,))
        data = db_cursor.fetchone()
        if data is not None:
            result = {'name': data[0],
                      'class_name': data[1],
                      'variable': data[2],
                      'ref_value': data[3],
                      'value_type': data[4],
                      'check_once': data[5],
                      'check_interval': data[6]
                      }
#            result.update({"data_store": "SQLite3 file:{}".format(self.db_file)})
            return result
        else:
            return None

    def get_all_conditions(self):
        """
        Gets a dictionary with all conditions in the configuration database.
        :return: a dictionary with the all conditions names as keys and None as value.
        """
        cursor = self.db.cursor()
        cursor.execute('select name from condition')
        result = {i[0]: None for i in cursor}
        return result

    def get_condition(self, name):
        """
        Creates a condition object from a variable stored in the configuration database.
        :param name: condition name.
        :return: a condition object.
        """
        config_data = Condition.get_dict_from_config(self, name)
        args = {a[0]: a[1]
                for a in config_data.items()
                if a[0] not in ['class_name', 'variable']}

        return create_implementation(config_data['class_name'], **args)

    # Actions

    def update_action(self, action):
        """
        Updates the action configuration data in the configuration database.
        :param action: variable object to be saved.
        :return: True on success, false otherwise.
        """
        logging.debug("action: {}".format(action.__dict__))
        result = False
        cursor = self.db.cursor()
        class_name = type(action).__name__
        rowcount = 0
        try:
            if action.condition is not None:
                cursor.execute(
                    """update {}
                        set
                        class_name = ?,
                        condition = ?
                        where name = ?""".format(Action.table),
                    (class_name, action.condition.name, action.name))
            else:
                cursor.execute(
                    """ update {}
                        set
                        class_name = ?,
                        condition = NULL
                        where name = ?""".format(Action.table),
                    (class_name, action.name))
            rowcount = cursor.rowcount
            if rowcount > 0:
                self.db.commit()
                result = True
        finally:
            logging.debug(rowcount)
        return result

    def save_action(self, action):
        """
        Creates (inserts) a new action in the configuration database.
        :param action: action object to be saved.
        :return: true on success, false otherwise.
        """

        rowcount = 0
        result = False
        cursor = self.db.cursor()
        class_name = type(action).__name__
        try:
            if action.__condition is not None:
                cursor.execute(
                    """ insert into {} 
                    (name, 
                    class_name,
                    condition
                    )
                    values (?,?,?)
                    """.format(Action.table),
                    (action.name, class_name, action.condition))
            else:
                cursor.execute(
                    """ insert into {} 
                    (name, 
                    class_name)
                    values (?,?)
                    """.format(Action.table),
                    (action.name, class_name))
                rowcount = cursor.rowcount
            if rowcount > 0:
                self.db.commit()
                result = True
        except:
            pass
        return result

    def get_action_dict(self, name):
        """
        Gets the configuration of an action as saved in the configuration database.
        :param name: the name of the action whose information is to be fetched.
        :return: a dictionary with the current information in the configuration database
        or None if the variable does not exist.
        """
        db_cursor = self.db.cursor()
        db_cursor.execute("""select name,
                            class_name,
                            condition
                            from {}
                            where name = ?""".format(Action.table),
                          (name,))
        data = db_cursor.fetchone()
        if data is not None:
            result = {'name': data[0],
                      'class_name': data[1],
                      'condition': data[2]
                      }
#            result.update({"data_store": "SQLite3 file:{}".format(self.db_file)})
            return result
        else:
            return None

    def get_all_actions(self):
        """
        Gets a dictionary with all actions in the configuration database.
        :return: a dictionary with the all actions names as keys and None as value.
        """
        cursor = self.db.cursor()
        cursor.execute('select name from action')
        result = {i[0]: None for i in cursor}
        return result

    def get_action(self, name):
        """
        Creates an action object from an action stored in the configuration database.
        :param name: action name.
        :return: a condition object.
        """
        config_data = Action.get_dict_from_config(self, name)
        args = {a[0]: a[1]
                for a in config_data.items()
                if a[0] not in ['class_name', 'condition']}

        return create_implementation(config_data['class_name'], **args)
