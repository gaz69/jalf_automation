#!/usr/bin/env python
# -*- coding: utf_8 -*-
"""
"""

import sys

import modbus_tk
import modbus_tk.defines as cst
from modbus_tk import modbus_tcp
import time
import random
import struct


if __name__ == "__main__":
    """main"""

    logger = modbus_tk.utils.create_logger(name="console", record_format="%(message)s")

    try:
        #Create the server
        server = modbus_tcp.TcpServer()
        logger.info("running...")
        logger.info("enter 'quit' for closing the server")

        random.seed()

        server.start()
        slave_1 = server.add_slave(1)
        slave_1.add_block('holding_registers', cst.HOLDING_REGISTERS, 0, 20000)
        print(f'done: block "holding_registers" added')
        slave = server.get_slave(1)

        slave.add_block("coils", cst.COILS, 0, 30)
        print(f'done: block "coils" added')

        while True:
            val = random.randint(0, 500)
            tmp = struct.pack('f', val)
            val1 = bytes((tmp[2], tmp[3], tmp[0], tmp[1]))
            tmp = struct.unpack('HH', val1)
            slave.set_values("holding_registers", 19002, tmp)
            values = slave.get_values("holding_registers", 19002, 2)
            sys.stdout.write('done: values written: %s\r\n' % str(values))
            print(f'done: values written: {val}')
            time.sleep(2)
    finally:
        server.stop()
