#!/usr/bin/env python

import sqlite3
import struct
import time, datetime
import logging
from controller.controller import Controller
from io_components.base.binding import Binding
from io_components.base.connector import Connector
from io_components.base.variable import Variable, VarType
from io_components.base.condition import *
from io_components.base.action import *
from data_stores.sqlite3_data_store import Sqlite3DataStore
from io_components.implementations.modbus import * #ModbusRtuConnector, ModbusTcpConnector, ModbusBinding
import serial

class ShowConfig(object):

    def __init__(self, controller):
        self._save_state = 'idle'
        self.controller = controller

    def show(self):

#       Variables
        for v in self.controller.variables:
            print(f'variable: {v}');

#       Connectors
        for v in self.controller.connectors:
          print(f'connector: {v}')

#       Bindings
        for v in self.controller.bindings:
          print(f'binding: {v}')

#       Conditions
        for v in self.controller.conditions:
          print(f'condition: {v}')

#       Actions
        for a in self.controller.actions:
          print(f'action: {a}')

class Observer():
    def notify (self, var):
        ahora = datetime.datetime.now().strftime("%H:%M:%S")
        print(f'{ahora}: {var.name} = {var.value}')

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    resources = Controller(Sqlite3DataStore('app_data/resources.db'))
    resources.load()
    resources.connect()

    show_config=ShowConfig(resources)
    show_config.show()

    observer = Observer()
    resources.variables["Modbus_19002"].observers.add(observer)

#    resources.connectors['localhost_Modbus'] = ModbusTcpConnector(name='localhost_Modbus', remote_host='localhost', remote_port=502)
    binding = ModbusBinding('coils', resources.connectors['localhost_Modbus'], 1, ModbusType.coil, 1)
    resources.bindings['coils'] = binding

    variable = Variable('coils', var_type=VarType.boolean, binding=binding, value=0)
    variable.observers.add(observer)
    resources.variables['coils'] = variable

    binding.start_polling()

    while True:
        val = resources.variables['coils'].value
        if val == 0:
            val = 1
        else:
            val = 0

        resources.variables['coils'].value = val
        time.sleep(2)

#    run_config_app()
#  cli.show()