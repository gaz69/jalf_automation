import abc
import json
from threading import Timer
import logging


class Binding(abc.ABC):
    """
    A Binding provides the means to connect a Variable to an external device. It "binds" a Variable to a Connector.
    Bindings are application specific and, in most cases, dependent of the communication protocol to used.
    This class is the abstract base class for all bindings and it is not design to be instantiated.
    """
    table = 'binding'

    def __init__(self,
                 name,
                 connector=None,
                 variable=None,
                 notifier=None
                 ):
        self.name = name
        self.connector = connector
        if variable is not None:
            self.variable = variable
        if notifier is not None:
            self.__notifier = notifier
        self.conditions = set()
        self.polling = False

    def start_polling(self):
        """
        Starts a thread which polls the remote device at regular intervals
        :return: nothing
        """
        self.polling = True
        self._poll()

    def stop_polling(self):
        """
        Stops the polling thread. After calling this method the variable will usually
        stop receiving update notifications.
        :return:
        """
        self.polling = False
        try:
            self.timer.cancel()
        except Exception:
            pass

    def _poll(self):
        logging.debug('{} poll on {}'.format(self.name, self.connector.name))
        try:
            self.connector.read(binding=self)
        except Exception as e:
            print(type(e).__name__)

        if self.polling:
            self.timer = Timer(self.poll_time, self._poll)
            self.timer.start()

    def write(self, data):
        """
        Sends data to the remote device. This method is called by the bound variable to update the remote device.
        :param data: the data to send
        :return: nothing
        """
        self.connector.write(binding=self, data=data)

    @property
    def notifier(self):
        return self.__notifier

    @notifier.setter
    def notifier(self, notifier):
        """
        This property registers the bound variable "notifier" method to provide change event notifications
        :param notifier: the "notifier" method of the bound variable
        :return: the registered "notifier" method of a variable
        """
        self.__notifier = notifier
        logging.debug("Binding {} notifer set for connector {}".format(self.name, self.connector.name))

#    def update_config(self, db_connection, data):
    def update_config(self, data_store, data):
        """
        Updates the binding configuration data in the configuration database.
        :param data_store: connection to database.
        :param data: binding specific data.
        :return: True on success, false otherwise.
        """

        return data_store.update_binding(self, data)

    @abc.abstractmethod
    def save_to_config(self, db_connection, data):
        """
        Creates (inserts) a new binding in the configuration database.
        :param db_connection: sqlite connection to a configuration database.
        :param data: binding specific data.
        :return: true on success, false otherwise.
        """
        result = False
        cursor = db_connection.cursor()
        class_name = type(self).__name__
        try:
            cursor.execute(
                """ insert into {} 
                    (name, 
                    class_name,
                    connector,
                    data
                    )
                    values (?,?,?,?)
                    """.format(Binding.table),
                (self.name, class_name, self.connector.name, data))
            rowcount = cursor.rowcount
            if rowcount > 0:
                db_connection.commit()
                result = True
        except Exception:
            pass

        return result

    @classmethod
    #    def create_from_config(cls, database, name):
    def create_from_config(cls, data_store, name):
        """
        Creates a binding object from a binding stored in the configuration database.
        :param data_store: a sqlite connection to a configuration database.
        :param name: binding name.
        :return: a binding object.
        """

        print(type(data_store))
        return data_store.get_binding(name)

    @classmethod
    #    def get_dict_from_config(cls, database, name):
    def get_dict_from_config(cls, data_store, name):
        """
        Gets the configuration of a binding as saved in the configuration database.
        :param data_store: a sqlite connection to a configuration database.
        :param name: the name of the binding whose information is to be fetched.
        :return: a dictionary with the current information in the configuration database
        or None if the binding does not exist.
        """
        return data_store.get_binding_dict(name)

    @classmethod
    #    def get_all (cls, database):
    def get_all(cls, data_store):
        """
      Gets a dictionary with all bindings in the configuration database.
      :param data_store: a sqlite connection to a configuration database.
      :return: a dictionary with the all binding names as keys and None as value.
      """
        return data_store.get_all_bindings()
