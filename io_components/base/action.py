import abc


class Action(abc.ABC):
    """
    Actions are the means by which the external observers "notified" of the occurrence of a condition.
    Examples of actions could be sending an SMS or a Fax, setting of an LED on a Console or
    activating an audio signal.
    Actions are usually but not exclusively used in connection with conditions. For each condition it is possible
    to define actions that are then executed when the condition is triggered. Actions can be initiated
    by other objects like connectors or bindings.
    Actions are system specific and should be implemented by subclassing this base class.
    """

    table="action"

    def __init__(self, name, condition=None):
        self.condition = condition
        self.name = name
        pass

    @abc.abstractmethod
    def do(self, condition, expected, actual, format_string):
        pass

    @classmethod
    def get_all(cls, data_store):
        return data_store.get_all_actions();

    @classmethod
    def get_dict_from_config(cls, data_store,name):
        return data_store.get_action_dict(name)

    @classmethod
    def create_from_config(cls, data_store, name):
        """
        Creates an action object from an action stored in the configuration database.
        :param data_store: configuration database.
        :param name: action name.
        :return: an action object.
        """
        return data_store.get_action(name)

    def update_config(self, data_store):
        data_store.update_action(self)


class WriteToConsoleAction(Action):
    """
    Action class to write a message to the console
    """
    def __init__(self, name, condition, **kwargs):
        super().__init__(name, condition)

    def do(self, condition, expected, actual, format_string=None):
        if format_string is None:
            format_string = "*** Condition Type:{} Expected:{} Received:{}"
        print(format_string.format(type(condition).__name__, expected, actual))


class SendSmsAction(Action):
    """
    Action class to send an SMS
    TODO: Implement
    """
    def __init__(self, name, condition, **kwargs):
        super().__init__(name, condition)

    def do(self, condition, expected, actual, format_string=None):
        pass
