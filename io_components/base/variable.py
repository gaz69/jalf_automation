import abc
import logging

from .. import create_implementation


class VarType:
    boolean = 1
    byte = 2
    int16 = 3
    int32 = 4
    int64 = 5
    float = 6
    str = 128

    @classmethod
    def all(cls):
        return [(name, value) for name, value in vars(cls).items() if not name.startswith('_')]


class Variable(abc.ABC):
    """
    Variables are used to store local program information as well as to communicate with external devices.
    Variables communicate with devices by using Bindings. A variable is tied to a Binding by means of the "binding"
    property.
    Variables are able to receive event notifications and update their values accordingly by using the "notifier"
    method.
    Related terms:
    Binding: ties a variable to an external device (usually by means of a connector)
    Observer: an object that observes the variable and (maybe) uses it's value
    Condition: a "special" kind of observer that monitors a variable and test for a condition
    """
    table = 'variable'

    def __init__(self, name, var_type, persistent=False, binding=None, value=None):
        self.__binding = None
        self.name = name
        self.var_type = var_type
        self.__value = value
        self.persistent = persistent
        self.binding = binding
        self.observers = set()
        self.conditions = set()

    def notifier(self, value):
        """
        Event notification method.
        When a variable is tied to a Binding, the "notifier" property of the binding is set to this method.
        This allows the binding to "notify" the variable of a change. When this method is called the variable
        receives a change notification and updates it's value.

        :param value: after a notification the variable will be set to this
        :return: nothing
        """
        if type(value) is tuple:
            self.__value = value[0]
        else:
            self.__value = value

        for observer in self.observers:
            observer.notify(self)

        for alarm in self.conditions:
            alarm.check(self.value)

    @property
    def value(self):
        return self.__value
    @value.setter
    def value(self, value):
        """
        This property provides access the variable value. It also calls the write method of
        the binding to send the value to the external device
        :param value: the value of the variable.
        :return: the current value.
        """
        self.__value = value
        self.binding.write(self.value)

    @property
    def binding(self):

        return self.__binding
    @binding.setter
    def binding(self, binding):
        """
        This property sets the Binding to be used for the variable to communicate with an external device.
        :param binding: the Binding object.
        :return: the Binding object when set.
        """
        logging.debug('Variable {} setting binding'.format(self.name))

        self.__binding = binding
        if self.__binding is not None:
            self.__binding.variable = self
            self.__binding.notifier = self.notifier

    def update_config(self, data_store, data=''):
        """
        Updates the variable configuration data in the configuration database.
        :param data_store: configuration database.
        :param data: variable specific data.
        :return: True on success, false otherwise.
        """
        return data_store.update_variable(self, data)

    def save_to_config(self, data_store, data=''):
        """
        Creates (inserts) a new variable in the configuration database.
        :param data_store: configuration database.
        :param data: variable specific data.
        :return: true on success, false otherwise.
        """
        data_store.save_variable(self, data)

    @classmethod
    #    def get_dict_from_config(cls,database, name):
    def get_dict_from_config(cls, data_store, name):
        """
        Gets the configuration of a variable as saved in the configuration database.
        :param data_store: configuration database.
        :param name: the name of the variable whose information is to be fetched.
        :return: a dictionary with the current information in the configuration database or None if the variable does not exist.
        """
        return data_store.get_variable_dict(name)

    @classmethod
    #    def get_all(cls, database):
    def get_all(cls, data_store):
        """
        Gets a dictionary with all variables in the configuration database.
        :param data_store: connection to database.
        :return: a dictionary with the all variable names as keys and None as value.
        """
        return data_store.get_all_variables()

    @classmethod
    def create_from_config(cls, data_store, name):
        """
        Creates a variable object from a variable stored in the configuration database.
        :param data_store: configuration database.
        :param name: variable name.
        :return: a variable object.
        """
        return data_store.get_variable(name)

    @classmethod
    def get_types_dict(cls):
        """
        Gets all currently known variable types
        :return: a dictionary with the name of the type as key and the numeric equivalent as value
        """
        return {"boolean": 1, "byte": 2, "int16": 3, "int32": 4, "int64": 5, "float": 6, "string": 128}
