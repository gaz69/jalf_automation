import abc


class Condition(abc.ABC):
    """
    Conditions provide the means to react to a specific condition in the system,
    To achieve this, one or more Conditions are registered to a variable, the value of the
    variable is then compared to the predefined values in the Condition object and if necessary
    an action is executed.
    This class should not be instantiated, instead there are several general purpose Condition
    classes predefined that can be use to monitor condition. If necessary the programmer can
    define his own Conditions by subclassing this class.
    See also: Action
    """

    table = "condition"

    def __init__(self, name,
                 variable=None,
                 ref_value=None,
                 value_type=None,
                 check_interval=None,
                 check_once=None):
        self.name = name
        self.last_value = 0
        self.is_set = False
        self.actions = set()
        self.ref_value = ref_value
        self.value_type = value_type
        self.variable = variable
        self.check_interval = check_interval
        self.check_once = check_once

    @abc.abstractmethod
    def check(self, value):
        pass

    def trigger(self, format_string=None):
        for action in self.actions:
            action.do(self, self.ref_value, self.last_value, format_string)

    @classmethod
    def get_all(cls, data_store):
        return data_store.get_all_conditions()

    @classmethod
    def get_dict_from_config(cls, data_store, name):
        return data_store.get_condition_dict(name)

    @classmethod
    def create_from_config(cls, data_store, name):
        """
        Creates a condition  object from a variable stored in the configuration database.
        :param data_store: configuration database.
        :param name: condition name.
        :return: a condition object.
        """
        return data_store.get_condition(name)

    def update_config(self, data_store):
        """
        Updates the condition configuration data in the configuration database.
        :param data_store: a configuration provider.
        :return: True on success, false otherwise.
        """

        return data_store.update_condition(self)


class BooleanCondition(Condition):
    def __init__(self, name, variable=None, ref_value=None, value_type=None, check_interval=None, check_once=None,
                 **kwargs):
        super().__init__(name, variable, ref_value, value_type, check_interval, check_once)

    def check(self, value):
        self.last_value = value
        if self.ref_value is False:
            if bool(value) is False:
                self.trigger()
        else:
            if bool(value):
                self.trigger()


class GreaterThanCondition(Condition):
    def __init__(self, name, variable=None, ref_value=None, value_type=None, check_interval=None, check_once=None,
                 **kwargs):
        super().__init__(name, variable, ref_value, value_type, check_interval, check_once)

    def check(self, value):
        self.last_value = value
        if self.last_value > self.ref_value:
            self.trigger()


class LessThanCondition(Condition):
    def __init__(self, name, variable=None, ref_value=None, value_type=None, check_interval=None, check_once=None,
                 **kwargs):
        super().__init__(name, variable, ref_value, value_type, check_interval, check_once)

    def check(self, value):
        self.last_value = value
        if self.last_value < self.ref_value:
            self.trigger()


class GreaterOrEqualCondition(Condition):
    def __init__(self, name, variable=None, ref_value=None, value_type=None, check_interval=None, check_once=None,
                 **kwargs):
        super().__init__(name, variable, ref_value, value_type, check_interval, check_once)

    def check(self, value):
        self.last_value = value
        if self.last_value >= self.ref_value:
            self.trigger()


class LessOrEqualCondition(Condition):
    def __init__(self, name, variable=None, ref_value=None, value_type=None, check_interval=None, check_once=None):
        super().__init__(name, variable, ref_value, value_type, check_interval, check_once)

    def check(self, value):
        self.last_value = value
        if self.last_value <= self.ref_value:
            self.trigger()


class ConnectionFailureCondition(Condition):
    def __init__(self, name, variable=None,
                 ref_value=None,
                 value_type=None,
                 check_interval=None,
                 check_once=None,
                 **kwargs):
        super().__init__(name, variable, ref_value, value_type, check_interval, check_once)

    def check(self, connected):
        if not connected:
            self.trigger()
