from enum import Enum
import logging
import abc
import utils

from .. import create_implementation


class State(Enum):
    """
    Enumeration with well defined states for a connector.
    """
    error = -1024
    connecting = -1
    disconnected = 0
    connected = 1


class Connector(abc.ABC):
    """
    Connectors provide communication channels to external devices.
    This class defines the base functionality common to all connectors.
    Developers should implement specific connectors for each device type by subclassing this class.
    """
    table = 'connector'

    def __init__(self, name):
        self.name = name
        self.state = State.disconnected
#        self.data_store = data_store
        self.conditions = set()

    @abc.abstractmethod
    def connect(self):
        """
        This method must be implemented to open the connection to a remote device.
        :return: implementation dependant.
        """
        return False

    @abc.abstractmethod
    def disconnect(self):
        """
        This method must be implemented to close the connection to a remote device
        :return: implementation dependant.
        """
        return False

    def clear_error(self):
        """
        Clears an error condition and reset the connection to disconnected
        :return: nothing
        """
        if self.state == State.error:
            self.state = State.disconnected

    @abc.abstractmethod
    def read(self, binding):
        """
        This method must be implemented to read data from the external device
        :param binding: the binding that requested the read (poll)
        :return: nothing
        """
        logging.debug('reading for binding {}'.format(binding.name))
        return

    @abc.abstractmethod
    def write(self, binding, data):
        """
        This method must be implemented to send data to the external device
        :param binding: the binding that requested the write
        :param data: the data to send
        :return: nothing
        """
        logging.debug(('writing for binding: {} data: {}'.format(binding.name, data)))

    def test(self):
        """
        Tests the connection to a remote device by opening and inmediately closing the connection.
        If a connection is already opened the method returns True.
        :return: True if the connection attempt was successful,otherwise False.
        """
        ret_val = False
        if self.state == State.disconnected:
            ret_val = self.connect()
            if ret_val:
                ret_val = self.disconnect()
        elif self.state == State.connected:
            logging.debug("Connector is connected. Connection test not executed")
            ret_val = True
        if ret_val:
            logging.debug("Connector {}: method test: {}".format(self.name, utils.bool_to_ok_fail(ret_val)))
        else:
            logging.debug("Connector {}: method test: {}".format(self.name, utils.bool_to_ok_fail(ret_val)))

    def update_config(self, data_store, data):
        """
        Updates the connector configuration data in the configuration database.
        :param data_store: configuration database.
        :param data: connector specific data.
        :return: True on success, false otherwise.
        """
        data_store.update_connector(self, data)

    @abc.abstractmethod
    def save_to_config(self, data_store, data):
        """
        Creates (inserts) a new connector in the configuration database.
        :param data_store: configuration database.
        :param data: connector specific data.
        :return: true on success, false otherwise.
        """
        data_store.save_connector(self, data)

    @classmethod
    def create_from_config(cls, data_store, name):
        """
        Creates a connector object from a connector stored in the configuration database.
        :param data_store: configuration database.
        :param name: connector name.
        :return: a connector object.
        """
        config_data = Connector.get_dict_from_config(data_store, name)
        args = {a[0]: a[1] for a in config_data.items() if a[0] != 'class_name'}

        return create_implementation(config_data['class_name'], **args)

    @classmethod
    def get_dict_from_config(cls, data_store, name):
        """
        Gets the configuration of a connector as saved in the configuration database.
        :param data_store: configuration database.
        :param name: the name of the connector whose information is to be fetched.
        :return: a dictionary with the current information in the configuration database
        or None if the variable does not exist.
        """
        return data_store.get_connector_dict(name)

    @classmethod
    def get_all(cls, data_store):
        """
        Gets a dictionary with all connectors in the configuration database.
        :param data_store: configuration database.
        :return: a dictionary with the all connector names as keys and None as value.
        """
        return data_store.get_all_connectors()
