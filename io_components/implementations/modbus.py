from io_components.base.binding import Binding
from io_components.base.connector import Connector, State
from io_components.base.variable import Variable, VarType
from modbus_tk import modbus_tcp, hooks, modbus_rtu, defines as cst

import logging
import unittest
import json
import utils
import struct
import binascii


class ModbusType:
    coil = cst.COILS  # 1
    discrete_input = cst.DISCRETE_INPUTS  # 2
    holding_register = cst.HOLDING_REGISTERS  # 3
    input_register = cst.ANALOG_INPUTS  # 4

    @classmethod
    def all(cls):
        return [(name, value) for name, value in vars(cls.items()) if not name.startswith('_')]


class ModbusBinding(Binding):
    def __init__(self,
                 name,
                 connector,
                 slave_id,
                 modbus_data_type,
                 starting_address,
                 poll_time=1,
                 var_count=1,
                 notifier=None,
                 **kwargs):
        super().__init__(name=name, connector=connector, notifier=notifier)
        if not isinstance(connector, Connector):
            raise TypeError

        self.slave_id = int(slave_id)
        self.starting_address = int(starting_address)
        self.var_count = int(var_count)
        self.modbus_data_type = int(modbus_data_type)
        self.poll_time = float(poll_time)
        self.polling = False

    def update_config(self, database):
        data = {'slave_id': self.slave_id,
                'starting_address': self.starting_address,
                'var_count': self.var_count,
                'modbus_data_type': self.modbus_data_type,
                'poll_time': self.poll_time}

        jason = json.dumps(data)
        return super().update_config(database, jason)

    def save_to_config(self, database):
        data = {'slave_id': self.slave_id,
                'starting_address': self.starting_address,
                'var_count': self.var_count,
                'modbus_data_type': self.modbus_data_type,
                'poll_time': self.poll_time}

        jason = json.dumps(data)
        return super().save_to_config(database, jason)


class ModbusProtocol:
    """
    High-Level (generic) class for Modbus
    Coils: readable and writable, 1 bit (off/on)
    Discrete Inputs: readable, 1 bit (off/on)
    Input Registers: readable, 16 bits (0 to 65,535), essentially measurements and statuses
    Holding Registers: readable and writable, 16 bits (0 to 65,535), essentially configuration values
    """

    def __init__(self,
                 device_type,
                 # slave_id = None,
                 modbus_master=None):
        super().__init__()
        self.protocol_name = "Modbus"
        self.device_type = device_type
        self.modbus_master = modbus_master
        # self.slave_id = slave_id

    def read_coils(self,
                   slave,
                   first_coil,
                   count):
        return self.modbus_master.execute(slave=slave,
                                          function_code=cst.READ_COILS,
                                          starting_address=first_coil,
                                          quantity_of_x=count)

    def read_discrete_inputs(self,
                             slave,
                             first_input,
                             count,
                             data_format=None):
        return self.modbus_master.execute(slave=slave,
                                          function_code=cst.READ_DISCRETE_INPUTS,
                                          starting_address=first_input,
                                          quantity_of_x=count,
                                          data_format=data_format)

    def write_single_coil(self,
                          slave,
                          coil,
                          value):
        return self.modbus_master.execute(slave=slave,
                                          function_code=cst.WRITE_SINGLE_COIL,
                                          starting_address=coil,
                                          output_value=value)

    def read_input_registers(self,
                             slave,
                             first_register,
                             count,
                             data_format=None):
        return self.modbus_master.execute(slave=slave,
                                          function_code=cst.READ_INPUT_REGISTERS,
                                          starting_address=first_register,
                                          quantity_of_x=count,
                                          data_format=data_format)

    def read_holding_registers(self,
                               slave,
                               first_register,
                               count,
                               data_format=None):
        logging.debug('{}.read_holding_registers'.format(self.__class__.__name__))
        return self.modbus_master.execute(slave=slave,
                                          function_code=cst.READ_HOLDING_REGISTERS,
                                          starting_address=first_register,
                                          quantity_of_x=count,
                                          data_format=data_format)

    def write_multiple_coils(self,
                             slave,
                             first_coil,
                             values):
        return self.modbus_master.execute(slave=slave,
                                          function_code=cst.WRITE_MULTIPLE_COILS,
                                          starting_address=first_coil,
                                          output_value=values)

    def write_single_holding_register(self,
                                      slave,
                                      register,
                                      value):
        return self.modbus_master.execute(slave=slave,
                                          function_code=cst.WRITE_SINGLE_REGISTER,
                                          starting_address=register,
                                          output_value=value)

    def write_multiple_registers(self,
                                 slave,
                                 first_register,
                                 values,
                                 data_format=None):
        return self.modbus_master.execute(slave=slave,
                                          function_code=cst.WRITE_MULTIPLE_REGISTERS,
                                          starting_address=first_register,
                                          output_value=values,
                                          data_format=data_format)


class ModbusTcpConnector(Connector, ModbusProtocol):
    def __init__(self, name, remote_host, remote_port, **kwargs):  # ,data_store): #,slave_id=None):
        self.remote_host = remote_host
        self.remote_port = remote_port

        Connector.__init__(self,
                           name=name)  # ,
        # data_store=data_store)
        ModbusProtocol.__init__(self,
                                device_type="Master",
                                # slave_id=slave_id,
                                modbus_master=modbus_tcp.TcpMaster(host=self.remote_host,
                                                                   port=int(self.remote_port)))

    #        hooks.install_hook("modbus_tcp.TcpMaster.before_connect", self.on_before_connect)
    #        hooks.install_hook("modbus_tcp.TcpMaster.after_connect", self.on_after_connect)
    #        hooks.install_hook("modbus_tcp.TcpMaster.before_close", self.on_before_close)
    #        hooks.install_hook("modbus_tcp.TcpMaster.after_close", self.on_after_close)
    #        hooks.install_hook("modbus_tcp.TcpMaster.before_send", self.on_before_send)
    #        hooks.install_hook("modbus_tcp.TcpServer.after_send", self.on_after_send)
    #        hooks.install_hook("modbus_tcp.TcpMaster.after_recv", self.on_after_recv)

    def connect(self):
        ret_val = False
        if self.state == State.disconnected:
            try:
                self.modbus_master.open()
                self.state = State.connected
                ret_val = True
            except:
                self.state = State.error
        else:
            logging.debug("Already connected")

        logging.debug("Connector {}: method connect: {}".format(self.name, utils.bool_to_ok_fail(ret_val)))
        return ret_val

    def disconnect(self):
        ret_val = False
        if self.state != State.disconnected:
            try:
                self.modbus_master.close()
                self.state = State.disconnected
                ret_val = True
            except:
                self.state = State.error
        else:
            logging.debug("Connector already disconnected")

        logging.debug("Connector {}: method disconnect: {}".format(self.name,
                                                                   utils.bool_to_ok_fail(ret_val)))
        return ret_val

    def read(self, binding):
        '''coil = cst.COILS                            #1
        discrete_input = cst.DISCRETE_INPUTS        #2
        holding_register = cst.HOLDING_REGISTERS    #3
        input_register = cst.ANALOG_INPUTS          #4
    '''
        data_format = None
        if binding.variable.var_type == VarType.float:
            data_format = ">f"
        elif binding.variable.var_type == VarType.int16:
            data_format = ">h"
        elif binding.variable.var_type == VarType.int32:
            data_format = ">l"
        elif binding.variable.var_type == VarType.int64:
            data_format = ">q"

        logging.debug(f'{self.__class__.__name__}.read Binding:{binding.name} read for datatype {binding.modbus_data_type} format {data_format}')
        if binding.modbus_data_type == 1:
            data = self.read_coils(binding.slave_id, binding.starting_address, binding.var_count)
        elif binding.modbus_data_type == 2:
            data = self.read_discrete_inputs(binding.slave_id, binding.starting_address, binding.var_count,
                                             data_format=data_format)
        elif binding.modbus_data_type == 3:
            data = self.read_holding_registers(binding.slave_id, binding.starting_address, binding.var_count,
                                               data_format=data_format)
        elif binding.modbus_data_type == 4:
            data = self.read_input_registers(binding.slave_id, binding.starting_address, binding.var_count,
                                             data_format=data_format)
        else:
            logging.debug('wrong datatype')
        logging.debug("{}.read: Result {}".format(self.__class__.__name__, data))
        binding.notifier(data)

    def write(self, binding, data):
        pass
        if binding.modbus_data_type == 1:
            if type(data) is tuple:
                data = self.write_multiple_coils(binding.slave_id, binding.starting_address, data)
            else:
                data = self.write_single_coil(binding.slave_id, binding.starting_address, data)
        elif binding.modbus_data_type == 2:
            print('writing to input coils not implemented')
            pass
        elif binding.modbus_data_type == 3:
            if type(data) is tuple:
                data = self.write_multiple_registers(binding.slave_id, binding.starting_address, data)
            else:
                data = self.write_single_holding_register(binding.slave_id, binding.starting_address, data)
        elif binding.modbus_data_type == 4:
            print('writing to input registers not implemented')
            pass

    def on_before_connect(self, args):
        master = args[0]
        logging.debug("on_before_connect {0} {1}".format(master._host, master._port))

    def on_after_connect(self, args):
        master = args[0]
        logging.debug("on_after_connect {0} {1}".format(master._host, master._port))

    def on_before_close(self, args):
        master = args[0]
        logging.debug("on_before_close {0} {1}".format(master._host, master._port))

    def on_after_close(self, args):
        master = args[0]
        logging.debug("on_after_close {0} {1}".format(master._host, master._port))

    def on_before_send(self, args):
        master = args[0]
        request = args[1]
        logging.debug("on_before_send {0}".format(request))
        logging.debug("on_before_send {0}".format(request))

    def on_after_send(self, args):
        master = args[0]
        request = args[1]
        logging.debug("on_after_send {0}".format(request))

    def on_after_recv(self, args):
        response = args[1]
        logging.debug("on_after_recv {0} bytes received".format(len(response)))
        logging.debug("on_after_recv response: {0}".format(response))

        data_len = response[8]
        data = response[9:9 + data_len + 1]

    def update_config(self, data_store):
        data = {'remote_host': self.remote_host,
                'remote_port': self.remote_port}

        jason = json.dumps(data)

        return super().update_config(data_store, jason)

    def save_to_config(self, database):
        data = {'remote_host': self.remote_host,
                'remote_port': self.remote_port}

        jason = json.dumps(data)
        return super().save_to_config(database, jason)


class ModbusTcpConnectorTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # subprocess.Popen(["python ../examples/mysimu.py"])
        # subprocess.Popen(["python", "../examples/mysimu.py"])
        pass

    def setUp(self):
        self.test_instance = ModbusTcpConnector(name="test", remote_host='localhost', remote_port=5020)
        self.test_instance.connect()
        logging.basicConfig(level=logging.DEBUG)
        logging.debug('connected')

    def tearDown(self):
        self.test_instance.disconnect()
        logging.debug("disconnected")

    def test_read_holding_registers(self):
        result = self.test_instance.read_holding_registers(slave=2, first_register=1, count=4)
        logging.debug("test {}".format(result))

        result = self.test_instance.read_holding_registers(slave=2, first_register=0, count=1)

        # Read and write floats
        self.test_instance.write_multiple_registers(
            slave=1,
            first_register=0,
            values=[3.14],
            data_format='>f')
        self.test_instance.read_holding_registers(1, 0, 2, data_format='>f')

        # send some queries
        # self.read_coils(first_coil=0,count=10)
        # self.read_discrete_inputs(first_input=0,count=8)
        # self.read_input_registers(first_register=100,count=3)
        # logger.info(master.execute(1, cst.READ_HOLDING_REGISTERS, 100, 12))
        # logger.info(master.execute(1, cst.WRITE_SINGLE_COIL, 7, output_value=1))
        # logger.info(master.execute(1, cst.WRITE_SINGLE_REGISTER, 100, output_value=54))
        # logger.info(master.execute(1, cst.WRITE_MULTIPLE_COILS, 0, output_value=[1, 1, 0, 1, 1, 0, 1, 1]))
        # logger.info(master.execute(1, cst.WRITE_MULTIPLE_REGISTERS, 100, output_value=xrange(12)))


class ModbusRtuConnector(Connector, ModbusProtocol):
    def __init__(self, name, serial, slave_id, **kwargs):
        self.serial = serial
        self.slave = slave_id

        Connector.__init__(self,
                           name=name)  # ,
        # data_store=data_store)
        ModbusProtocol.__init__(self,
                                device_type="Master",
                                # slave_id=slave_id,
                                modbus_master=modbus_rtu.RtuServer(self.serial))
    #        hooks.install_hook("modbus_tcp.TcpMaster.before_connect", self.on_before_connect)
    #        hooks.install_hook("modbus_tcp.TcpMaster.after_connect", self.on_after_connect)
    #        hooks.install_hook("modbus_tcp.TcpMaster.before_close", self.on_before_close)
    #        hooks.install_hook("modbus_tcp.TcpMaster.after_close", self.on_after_close)
    #        hooks.install_hook("modbus_tcp.TcpMaster.before_send", self.on_before_send)
    #        hooks.install_hook("modbus_tcp.TcpServer.after_send", self.on_after_send)
    #        hooks.install_hook("modbus_tcp.TcpMaster.after_recv", self.on_after_recv)

    def connect(self):
        ret_val = False
        if self.state == State.disconnected:
            try:
                self.modbus_master.open()
                self.state = State.connected
                ret_val = True
            except:
                self.state = State.error
        else:
            logging.debug("Already connected")

        logging.debug("Connector {}: method connect: {}".format(self.name, utils.bool_to_ok_fail(ret_val)))
        return ret_val

    def disconnect(self):
        ret_val = False
        if self.state != State.disconnected:
            try:
                self.modbus_master.close()
                self.state = State.disconnected
                ret_val = True
            except:
                self.state = State.error
        else:
            logging.debug("Connector already disconnected")

        logging.debug("Connector {}: method disconnect: {}".format(self.name,
                                                                   utils.bool_to_ok_fail(ret_val)))
        return ret_val

    def read(self, binding):
        '''coil = cst.COILS                            #1
        discrete_input = cst.DISCRETE_INPUTS        #2
        holding_register = cst.HOLDING_REGISTERS    #3
        input_register = cst.ANALOG_INPUTS          #4
    '''
        data_format = None
        if binding.variable.var_type == VarType.float:
            data_format = ">f"
        elif binding.variable.var_type == VarType.int16:
            data_format = ">h"
        elif binding.variable.var_type == VarType.int32:
            data_format = ">l"
        elif binding.variable.var_type == VarType.int64:
            data_format = ">q"

        logging.debug(f'{self.__class__.__name__}.read Binding:{binding.name} read for datatype {binding.modbus_data_type} format {data_format}')
        if binding.modbus_data_type == 1:
            data = self.read_coils(binding.slave_id, binding.starting_address, binding.var_count)
        elif binding.modbus_data_type == 2:
            data = self.read_discrete_inputs(binding.slave_id, binding.starting_address, binding.var_count,
                                             data_format=data_format)
        elif binding.modbus_data_type == 3:
            data = self.read_holding_registers(binding.slave_id, binding.starting_address, binding.var_count,
                                               data_format=data_format)
        elif binding.modbus_data_type == 4:
            data = self.read_input_registers(binding.slave_id, binding.starting_address, binding.var_count,
                                             data_format=data_format)
        else:
            logging.debug('wrong datatype')
        logging.debug("{}.read: Result {}".format(self.__class__.__name__, data))
        binding.notifier(data)

    def write(self, binding, data):
        pass

    def on_before_connect(self, args):
        master = args[0]
        logging.debug("on_before_connect {0} {1}".format(master._host, master._port))

    def on_after_connect(self, args):
        master = args[0]
        logging.debug("on_after_connect {0} {1}".format(master._host, master._port))

    def on_before_close(self, args):
        master = args[0]
        logging.debug("on_before_close {0} {1}".format(master._host, master._port))

    def on_after_close(self, args):
        master = args[0]
        logging.debug("on_after_close {0} {1}".format(master._host, master._port))

    def on_before_send(self, args):
        master = args[0]
        request = args[1]
        logging.debug("on_before_send {0}".format(request))
        logging.debug("on_before_send {0}".format(request))

    def on_after_send(self, args):
        master = args[0]
        request = args[1]
        logging.debug("on_after_send {0}".format(request))

    def on_after_recv(self, args):
        response = args[1]
        logging.debug("on_after_recv {0} bytes received".format(len(response)))
        logging.debug("on_after_recv response: {0}".format(response))

        data_len = response[8]
        data = response[9:9 + data_len + 1]

    def update_config(self, data_store):
        data = {'remote_host': self.remote_host,
                'remote_port': self.remote_port}

        jason = json.dumps(data)

        return super().update_config(data_store, jason)

    def save_to_config(self, database):
        data = {'remote_host': self.remote_host,
                'remote_port': self.remote_port}

        jason = json.dumps(data)
        return super().save_to_config(database, jason)

