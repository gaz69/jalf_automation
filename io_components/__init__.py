import pkgutil
from importlib import import_module

def create_implementation(class_name,**args):
    path = pkgutil.extend_path(__path__, __name__)
    instance = None
    for importer, modname, ispkg in pkgutil.walk_packages(path=path, prefix=__name__+'.'):
        # search for classes to be instantiated only in
        # io_components.base and in
        # io_components.implementations
        if modname.find('implementations.') >= 0 or \
         modname.find('base.') >=0:
            # print(modname)
            module = import_module(modname)
            cls = getattr(module, class_name,None)
            if cls is not None:
                instance = cls(**args)
                break
    return instance


