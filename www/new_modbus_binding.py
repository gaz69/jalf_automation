#!/usr/bin/env python3


import sqlite3
#import inspect
#from src.io_implementations.modbus import ModbusBinding
import cgi
import cgitb
import importlib.util

cgitb.enable(display=1,logdir='/home/juan/errors')

form = cgi.FieldStorage()

binding_name = ''
connector_name = ''
slave_id = ''
modbus_data_type = ''
starting_address = ''
poll_interval = ''
variable_count = ''
error_msg= ''

if len(form) > 0:
    error_msg = ''
    for name in ['binding_name',
                 'connector_name',
                 'slave_id',
                 'modbus_data_type',
                 'starting_address',
                 'poll_interval',
                 'variable_count']:
        if (name not in form):
            error_msg = 'Not all values entered'
    if error_msg == '':
        binding_name = form.getvalue("binding_name")
        connector_name = form.getvalue("connector_name")
        slave_id = form.getvalue("slave_id")
        modbus_data_type = form.getvalue("modbus_data_type")
        starting_address = form.getvalue("starting_address")
        poll_interval = form.getvalue("poll_interval")
        variable_count = form.getvalue("variable_count")

        spec = importlib.util.spec_from_file_location("modbus", "../io_implementations/modbus.py")
        modbus = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(modbus)

        conn = sqlite3.connect('./../app_data/resources.db')

        binding = modbus.ModbusBinding(name=binding_name,
                                connector=connector_name,
                                slave_id=slave_id,
                                modbus_data_type=modbus_data_type,
                                starting_address=starting_address,
                                poll_time=poll_interval,
                                var_count=variable_count).save_to_config(conn,'bindings')




html = """Content-type: text/html\n\n

<html>
<head>
    <title>New ModbusBinding</title>
    <style>
        table, th, td {{border: 1px solid black;
                    border-collapse: collapse;
                    }}
    </style>
</head>
<body style="font-family:arial">
    <h2>New ModbusBinding</h2>
    <form action='/automation/www/new_modbus_binding.py' method='post'>
         <table>
         <tr><td>Name:</td><td><input type='text' name ="binding_name" value= "{}"/></td></tr>
         <tr><td>Connector:</td><td><input type='text' name = "connector_name" value="{}"/></td></tr>
         <tr><td>Slave Id:</td><td><input type='text' name = "slave_id" value="{}"/></td></tr>
         <tr><td>Modbus Data Type:</td><td><input type='text' name = "modbus_data_type" value="{}"/></td></tr>
         <tr><td>Starting Address:</td><td><input type='text' name = "starting_address" value="{}"/></td></tr>
         <tr><td>Poll interval</td><td><input type='text' name = "poll_interval" value="{}"/></td></tr>
         <tr><td>Variable Count :</td><td><input type='text' name = "variable_count" value="{}"/></td></tr>
         <tr><td><input type='submit' value='create_binding'</input></td><td><input type='submit' value='create_binding'</input></td></tr>
         </table>        
    </form>
    {}
<body>
</html>
""".format(binding_name,
           connector_name,
           slave_id,
           modbus_data_type,
           starting_address,
           poll_interval,
           variable_count,
           error_msg)

print(html)


def create_binding(class_name, **constructor_kwargs):
    binding = globals()[class_name](constructor_kwargs)

