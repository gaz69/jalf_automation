#!/usr/bin/env python3


import sqlite3

#import inspect


def get_table(table):
    result = '<table>{}</table>'
    conn = sqlite3.connect(database="../app_data/resources.db")
    cursor = conn.cursor()

    cursor.execute('PRAGMA table_info({})'.format(table))
    column_names=cursor.fetchall()

    cursor.execute('select * from {}'.format(table))
    data = cursor.fetchall()

    conn.close()

    content = ''
    td = ''
    for n in column_names :
        td += f'<th>{n[1]}</th>'

    tr = f'<tr>{td}</tr>'
    content += tr

    for d in data:
        td = ''
        for i in d:
            td += f'<td>{i}</td>'
        tr = f'<tr>{td}</tr>'
        content += tr
    result=result.format(content)
    return result

html = """Content-type: text/html\n\n
 
<html>
<head>
    <title>Automation Config</title>
    <style>
        table, th, td {{border: 1px solid black;
                    border-collapse: collapse;
                    }}
    </style>
</head>
<body style="font-family:arial">
    <h2>Current Configuration</h2>
    <h3>connectors</h3>
    {0}
    <a href='/automation/www/new_modbus_tcp_connector.py'>new connector</a>
    <h3>bindings</h3>
    {1}
    <a href='/automation/www/new_modbus_binding.py'>new binding</a>
    <h3>variables</h3>
    {2}
    <a href='/automation/www/new_variable.py'>new variable</a>
<body>
</html>
""".format(get_table('connectors'),get_table('bindings'),get_table('variables'))

print(html)

def create_binding(class_name,**constructor_kwargs):
    binding = globals()[class_name](constructor_kwargs)

