#!/usr/bin/env python3


import sqlite3

import inspect


html = """Content-type: text/html\n\n

<html>
<head>
    <title>New Binding</title>
    <style>
        table, th, td {{border: 1px solid black;
                    border-collapse: collapse;
                    }}
    </style>
</head>
<body style="font-family:arial">
    <h2>New Variable</h2>
    <form action='/automation/www/new_variable.py' method='post'>
        <table>
          <tr><td>Name:</td><td><input type='text' name="variable_name"/></td></tr>
          <tr><td>Variable Type:</td><td><input type='text' name="variable_type"/></td></tr>
          <tr><td></td>
          <td><input type='submit' value='create_variable'</input></td></tr>
        <table>
    </form>
<body>
</html>
"""
print(html)


def create_binding(class_name, **constructor_kwargs):
    binding = globals()[class_name](constructor_kwargs)

