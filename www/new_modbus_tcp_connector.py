#!/usr/bin/env python3


import sqlite3

import inspect


html = """Content-type: text/html\n\n

<html>
<head>
    <title>New Connector</title>
    <style>
        table, th, td {{border: 1px solid black;
                    border-collapse: collapse;
                    }}
    </style>
</head>
<body style="font-family:arial">
    <h2>New Modbus Tcp Connector</h2>
    <form action='/automation/www/new_modbus_tcp_connector.py' method='post'>
        <table>
          <tr><td>Name:</td><td><input type='text' name = "connector_name"/></td></tr>
          <tr><td>Host/IP:</td><td><input type='text' name = "host"/></td></tr>
          <tr><td>Port:</td><td><input type='text' name = "port"/></td></tr>
          <tr><td></td>
            <td><input type='submit' value='create_connector'</input></td></tr>
        </table>
    </form>
<body>
</html>
"""
print(html)


def create_binding(class_name, **constructor_kwargs):
    binding = globals()[class_name](constructor_kwargs)
