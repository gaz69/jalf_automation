#!/usr/bin/env python

#import tkinter_gui.new_modbus_tcp_connector as new_connector
#import tkinter_gui.new_modbus_binding as new_binding
#import tkinter_gui.new_variable as new_variable
#import tkinter_gui.main_screen as main_screen
#from qt_gui.frm_main_config import FrmMainConfig
#from qt_gui.frm_run_app import FrmRunApp

#------------------------------------------------------------------------------
#Project specific imports
#------------------------------------------------------------------------------
from controller.controller import Controller
from gui.qt_gui.main_window import MainWindow
from gui.curses_gui import cli as cli
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
#Other imports
#------------------------------------------------------------------------------
import logging
from PyQt5 import QtWidgets
import sys
import sqlite3
import os
#------------------------------------------------------------------------------

db = None
app_resources = None

def show_config(app,main_window):
    main_window.show_config()
    main_window.show()
    sys.exit(app.exec_())

def show_run(app,main_window):
    main_window.show_run()
    main_window.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG,
#    logging.basicConfig(level=logging.INFO,
                        filename=os.path.expanduser('~')+'/automation.log',
                        format='%(asctime)s %(message)s')

    db = sqlite3.connect('./app_data/resources.db')
    app_resources = Controller()

#    app_resources.run()

#    ui = "curses"
    ui = "qt"
    if (ui == "qt"):
      app = QtWidgets.QApplication(sys.argv)
      main_window = MainWindow(db,app_resources)
#      show_run(app,main_window)
      show_config(app,main_window)
    elif (ui == "curses"):
      cli.show()
      
